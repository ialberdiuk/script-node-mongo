const environment = process.env;
const path = require('path');
const apiService = require('./api.service');
const utils = require('../utils');

const extractDate = (param) => ({ date1: param.split('/')[4], date2: param.split('/')[5].split('?')[0] });

const createBodyText = (type) => {
  if (type === 'Monthly_Difference') {
    const currentMonth = extractDate(apiService.getDates('current_month'));
    const previousMonth = extractDate(apiService.getDates('previous_month'));
    return `Please, find attached latest weekly report from ${currentMonth.date2} to ${currentMonth.date1} 
    compared with previous month from ${previousMonth.date2} to ${previousMonth.date1}`;
  }
  if (type === 'User') {
    return 'Attached are the SIMs that have generated CDRs while in a suspended status';
  }
  if (type === 'Limits') {
    return 'Attached are the SIMs that have generated CDRs after the limit was exceeded';
  }
  if (type === 'Exceed') {
    return 'Attached are the SIMs that have reached the 110% (or over) of the limit';
  }
  if (type === 'Exceeded_Aggregate') {
    return 'Attached are the pooled/aggregated products that have reached the 110% (or over) of the limit for all their SIMs';
  }
};

exports.sendEmail = (type) => new Promise((resolve, reject) => {
  const currentDate = utils.getCurrentDate();
  const successText = createBodyText(type);
  let attachment;
  let isProcessSuccess = true;
  let typeReport;
  let text;
  const root = process.cwd();

  if (typeof type === 'object') {
    isProcessSuccess = false;
  }

  if (isProcessSuccess) {
    typeReport = type === 'Limits' ? '' : type;
    if (type === 'Exceed' || type === 'Exceeded_Aggregate') {
      attachment = path.join(`${root}/${type}_SIMs_Report_${currentDate}.csv`);
      text = `Alert Data Limit ${typeReport}`;
    } else if (type === 'Monthly_Difference') {
      attachment = path.join(`${root}/${type}_Report_${currentDate}.csv`);
      text = `Week Alert ${typeReport}`;
    } else {
      attachment = path.join(`${root}/Suspended_${type}_SIMs_Report_${currentDate}.csv`);
      text = `Alert Data Limit ${typeReport} Suspension`;
    }
  }

  const api_key = environment.API_KEY_MAILGUN;
  const domain = environment.DOMAIN;
  const mailgun = require('mailgun-js')({ apiKey: api_key, domain });
  const data = {
    from: environment.EMAIL_FROM,
    to: environment.EMAIL_TO,
    subject: isProcessSuccess === true ? text : `Error Alert Data ${type.report}`,
    text: isProcessSuccess === true ? successText : type.message,
    attachment,
  };

  mailgun.messages().send(data, (error, body) => {
    if (error) {
      console.log(error);
      reject(error);
    }
    console.log(body);
    resolve(body);
  });
});
