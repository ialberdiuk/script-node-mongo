const db = require('./db');
const report = require('./reports/check');
require('dotenv').config();

async function connect() {
  const conn = await db.connect();
  return conn;
}

const run = async (param) => {
  try {
    console.log('...Connecting to DB, be patient');
    const conn = await connect();
    if (param !== undefined) {
      await report.checkMonthlyDiff(conn);
      process.exit(0);
    } else {
      await report.checkOverUsage(conn);
      process.exit(0);
    }
  } catch (e) {
    console.log(e);
    process.exit(0);
  }
};

/* It means a third param is passed so weekly report will be executed */
run(process.argv[2]);
