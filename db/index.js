const mongoose = require('mongoose');
const mongo = require('../config/db.config');

let connection = null;

exports.connect = async () => {
  try {
    connection = await mongoose.connect(mongo.uri, { useNewUrlParser: true });
    return connection;
  } catch (e) {
    console.log(e, 'Error connection to DB');
  }
};
