const https = require('https');
const moment = require('moment');
require('dotenv').config();

const environment = process.env;

exports.getDates = (param) => {
  let initialDate;
  let finalDate;
  const accountId = environment.ACCOUNT_ID;
  if (param === 'previous_month') {
    initialDate = moment().subtract(1, 'months').date(1).format('YYYY-MM-DD');
    finalDate;
    const currentDate = new Date();
    const month = currentDate.getMonth();
    const day = currentDate.getDate();
    if ((month === 1 && day === 28 || month === 1 && day === 29) || (day === 30 || day === 31)) {
      finalDate = (moment().subtract(1, 'months')).subtract(1, 'day').endOf('month')
        .format('YYYY-MM-DD');
    } else {
      finalDate = (moment().subtract(1, 'months')).subtract(1, 'day').format('YYYY-MM-DD');
    }
    return (`/v3/cdr-aggregations/aggregation/${initialDate}/${finalDate}?` + `accountId=${accountId}`);
  }
  if (param === 'current_month') {
    initialDate = moment().startOf('month').format('YYYY-MM-DD');
    finalDate = moment().subtract(1, 'days').format('YYYY-MM-DD');
    return (`/v3/cdr-aggregations/aggregation/${initialDate}/${finalDate}?` + `accountId=${accountId}`);
  }
};

exports.sendRequest = (accounts, month) => new Promise((resolve, reject) => {
  const data = `{"filter":{"accountId":${JSON.stringify(accounts)}},"groupBy":["bundleId"]}`;
  const options = {
    hostname: environment.API_ENDPOINT,
    port: 443,
    path: this.getDates(month),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': String(data.length),
      'x-api-key': environment.X_API_KEY,
    },
  };
  let body = [];
  const req = https.request(options, (res) => {
    res.on('data', (d) => {
      body.push(d);
    });

    res.on('end', () => {
      body = JSON.parse(Buffer.concat(body).toString());
      resolve({ statusCode: res.statusCode, result: body });
    });
  });

  req.on('error', (e) => {
    reject(e);
  });

  req.write(data);
  req.end();
});
