const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const moment = require('moment');

exports.convertToArray = async (data) => {
  const result = await data.toArray();
  if (result.length > 0) {
    return result;
  }

  throw ({ error: 'No data found', message: 'There should not be any SIM with over usage' });
};

const setHeader = (type) => {
  const data = [
    { id: 'ICCID', title: 'ICCID' },
    { id: 'accountId', title: 'Account' },
    { id: 'accountName', title: 'Account Name' },
    { id: 'defaultEmail', title: 'Default Email' },
    { id: 'network', title: 'Network' },
    { id: 'amountCDR', title: 'Amount of CDR' },
    { id: 'bytes', title: 'Amount of usage (MB)' },
    { id: 'name', title: 'Product name' },
    { id: 'productType', title: 'Product Type' },
    { id: 'poolAmount', title: 'Pool Amount (MB)' },
    { id: 'totalDataUsed', title: 'Total Data Used' },
    { id: 'poolUtilization', title: 'Pool Utilization' },
    { id: 'limit', title: 'Limit' },
    { id: 'variation', title: 'Increase' },
  ];

  if (type === 'Exceeded_Aggregate') {
    return data.filter((elem) => (elem.id !== 'limit' && elem.id !== 'ICCID'
      && elem.id !== 'accountId' && elem.id !== 'amountCDR' && elem.id !== 'bytes' && elem.id !== 'variation'));
  }
  if (type === 'User') {
    return data.filter((elem) => (elem.id !== 'limit' && elem.id !== 'name' && elem.id !== 'poolUtilization'
      && elem.id !== 'poolAmount' && elem.id !== 'productType' && elem.id !== 'totalDataUsed' && elem.id !== 'variation'));
  }
  if (type === 'Limits') {
    return data.filter((elem) => (elem.id !== 'bytes' && elem.id !== 'name' && elem.id !== 'poolUtilization'
      && elem.id !== 'poolAmount' && elem.id !== 'productType' && elem.id !== 'totalDataUsed' && elem.id !== 'variation'));
  }
  if (type === 'Exceed') {
    return data.filter((elem) => (elem.id !== 'amountCDR' && elem.id !== 'name' && elem.id !== 'poolUtilization'
      && elem.id !== 'poolAmount' && elem.id !== 'productType' && elem.id !== 'totalDataUsed' && elem.id !== 'variation'));
  }
  if (type === 'Monthly_Difference') {
    return data.filter((elem) => (elem.id !== 'amountCDR' && elem.id !== 'poolUtilization'
      && elem.id !== 'poolAmount' && elem.id !== 'productType' && elem.id !== 'totalDataUsed'
      && elem.id !== 'limit' && elem.id !== 'ICCID' && elem.id !== 'totalDataUsed'
      && elem.id !== 'bytes' && elem.id !== 'accountId'));
  }
};

exports.getCurrentDate = () => moment().format('YYYY-MM-DD');

exports.createCSV = (data, type) => {
  const exceededFileName = `${type}_SIMs_Report_${this.getCurrentDate()}.csv`;
  const suspendedFileName = `Suspended_${type}_SIMs_Report_${this.getCurrentDate()}.csv`;
  const monthlyDiffFileName =  `${type}_Report_${this.getCurrentDate()}.csv`;
  const fileName = type.includes('Exceed') ? exceededFileName : suspendedFileName;

  const csvWriter = createCsvWriter({
    path: type === 'Monthly_Difference' ? monthlyDiffFileName : fileName,
    header: setHeader(type),
  });

  return csvWriter
    .writeRecords(data)
    .then(() => console.log('The CSV file was written successfully'))
    .catch((e) => console.log(e, ' The CSV creation failed'));
};

exports.compareBundles = (bundlesFirstMonth, bundlesSecondMonth) => {
  const arrValues = [];
  let valueBundleSecondMonth;
  for (const [key, value] of bundlesFirstMonth) {
    if (bundlesSecondMonth.has(key)) {
      valueBundleSecondMonth = bundlesSecondMonth.get(key);
      if (valueBundleSecondMonth / value >= 2) {
        arrValues.push({ bundleId: key, dataUsed: value, variation: valueBundleSecondMonth / value * 100 });
      }
      if (value / valueBundleSecondMonth >= 2) {
        arrValues.push({ bundleId: key, dataUsed: value, variation: valueBundleSecondMonth / value * 100 });
      }
    }
  }

  return arrValues;
};

exports.joinValues = (data, products) => {
  const arrValues = [];
  const productsMap = new Map(products.map((key) => [key.accountTransferId, key.name]));
  const bundleMap = new Map(products.map((key) => [key.accountTransferId, key.bundleId]));
  const accountMap = new Map(products.map((key) => [key.accountTransferId, key.accountTransferId]));
  const networkMap = new Map(products.map((key) => [key.accountTransferId, key.network]));
  const resultsMap = new Map(products.map((key) => [key.accountTransferId, key.variation]));
  for (const item of data) {
    if (accountMap.has(item.id)) {
      arrValues.push({
        bundleId: bundleMap.get(item.id),
        accountName: item.accountName,
        defaultEmail: item.defaultEmail,
        name: productsMap.get(item.id),
        network: networkMap.get(item.id),
        variation: resultsMap.get(item.id),
      });
    }
  }

  return arrValues;
};
