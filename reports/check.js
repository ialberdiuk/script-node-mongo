const utils = require('../utils');
const service = require('../services/email.service');
const api = require('../services/api.service');
const query = require('../db/queries');

const getAssets = async (conn, type) => {
  if (type === 'User') {
    console.log('...Start report for suspensions by user');
    const data = await query.getSuspendedByUser(conn);
    const assets = await utils.convertToArray(data);
    return assets;
  }
  if (type === 'Limits') {
    console.log('...Start report for suspensions by limits');
    const data = await query.getSuspendedByLimit(conn);
    const assets = await utils.convertToArray(data);
    return assets;
  }
  if (type === 'Exceed') {
    console.log('...Start report for exceeding 110%');
    const data = await query.getLimitExceeded(conn);
    const assets = await utils.convertToArray(data);
    return assets;
  }
  if (type === 'Exceeded_Aggregate') {
    console.log('...Start report for exceeding aggregate/shared product type');
    const data = await query.getLimitPoolExceeded(conn);
    const assets = await utils.convertToArray(data);
    const products = await query.getProductType(assets, conn);
    return products;
  }
};

const getAccountValues = async (conn, type, data) => {
  if (type !== 'Exceeded_Aggregate') {
    return await query.getValuesByAccount(conn, data);
  }
  return await query.getValuesByProductType(conn, data);
};

const execProccessesToSendReports = async (conn, type) => {
  try {
    const data = await getAssets(conn, type);
    const values = await getAccountValues(conn, type, data);
    await utils.createCSV(values, type);
    await service.sendEmail(type);
  } catch (error) {
    await service.sendEmail({ error, message: error.message, report: type });
  }
};

exports.checkOverUsage = async (conn) => Promise.all([
  execProccessesToSendReports(conn, 'User'),
  execProccessesToSendReports(conn, 'Limits'),
  execProccessesToSendReports(conn, 'Exceed'),
  execProccessesToSendReports(conn, 'Exceeded_Aggregate'),
]);

const execProcessesToSendWeeklyReport = async (conn, type) => {
  try {
    const data = await query.getChildrenAccounts(conn);
    const accounts = data.map((elem) => (elem.id));
    const bundlesIdPreviousMonth = await api.sendRequest(accounts, 'previous_month');
    const arrFirstMonth = bundlesIdPreviousMonth.result.values;
    const bundlesFirstMonth = new Map(arrFirstMonth.map((key) => [key.id.bundleId, key.dataUsed]));
    const bundlesIdCurrentMonth = await api.sendRequest(accounts, 'current_month');
    const arrSecondMonth = bundlesIdCurrentMonth.result.values;
    const bundlesSecondMonth = new Map(arrSecondMonth.map((key) => [key.id.bundleId, key.dataUsed]));
    const results = utils.compareBundles(bundlesFirstMonth, bundlesSecondMonth);
    const products = await query.getProductName(conn, results);
    const values = utils.joinValues(data, products);
    await utils.createCSV(values, type);
    await service.sendEmail(type);
  } catch (error) {
    console.log(error);
    await service.sendEmail({ error, message: error.message, report: type });
  }
};

exports.checkMonthlyDiff = async (conn) => await execProcessesToSendWeeklyReport(conn, 'Monthly_Difference');
