# Alerts Data Limit

This repo is a handy project written in NODE JS to know when SIMs are/were used after limit was exceeded or state changed from active to suspended

It runs from a cronjob of K8s after the image is pushed to AWS ECR

It won´t work, I took this project from a real project but all secrets (.env) are fake. it is just a code example

<b>I thought it was a good example as queries.js (db folder) has a lot Mongo Queries, some of them are a bit complex</b>

<b>Also, this project relies heavily on Factory Pattern using the report type param</b>

<b>Interesting and handy use of Promises (concurrently execution), have a look at index.js line 52</b>

```
exports.checkOverUsage = async (conn) => Promise.all([
  execProccessesToSendReports(conn, 'User'),
  execProccessesToSendReports(conn, 'Limits'),
  execProccessesToSendReports(conn, 'Exceed'),
  execProccessesToSendReports(conn, 'Exceeded_Aggregate'),
]);

```
### Propose

1. Daily summary of the sim suspended that have received CDRs

2. Daily summary of the sims that has reached the 110% of the limit

3. Daily summary of pooled/aggregate products with 110% of the sum of limits

4. Weekly a summary with the difference of bundles vs previous month

### Installation

npm i

### Run alerts data limits when SIMs have been suspended by user (see commmits - ticket number - for reference)

npm start

### Build image talk to devops to upload to ECR

- docker build . -t <your username>/node-web-app
