const ObjectId = require('mongodb').ObjectID;
const unitMB = 1048576

exports.getSuspendedByUser = async (conn) => {
  const collection = conn.connection.collection('assetsimcards');
  return collection.find({
    status: 'suspended',
    suspensionDate: { $gt: new Date('2021-01-01') },
    $expr: {
      $gt: [
        '$lastCall.endTime',
        {
          $add: ['$suspensionDate', 300000],
        },
      ],
    },
  },
  {
    projection: {
      status: 1, iccid: 1, limit: 1, lastCall: 1, suspensionDate: 1, ownerAccountId: 1,
    },
  });
};

exports.getSuspendedByLimit = async (conn) => {
  const collection = conn.connection.collection('assetsimcards');
  return collection.find({
    carrierStatus: 'suspended',
    carrierSuspensionDate: { $gt: new Date('2021-01-01') },
    $expr: {
      $gt: [
        '$lastCall.endTime',
        {
          $add: ['$carrierSuspensionDate', 300000],
        },
      ],
    },
  },
  {
    projection: {
      status: 1, iccid: 1, limit: 1, lastCall: 1, suspensionDate: 1, ownerAccountId: 1,
    },
  });
};

exports.getLimitExceeded = async (conn) => {
  const collection = conn.connection.collection('assetsimcards');
  return collection.aggregate([
    {
      $match: {
        status: 'active',
        limit: { $gt: 0 },
        'subscriptions.bundles.0.dataUsed': { $exists: true, $gt: 0 },
      },
    },
    {
      $unwind: {
        path: '$subscriptions',
      },
    },
    {
      $unwind: {
        path: '$subscriptions.bundles',
      },
    },
    {
      $match: {
        $expr: {
          $gt: ['$subscriptions.bundles.dataUsed', { $multiply: ['$limit', 1.1] }],
        },
      },
    },
    {
      $sort: {
        iccid: 1,
      },
    },
  ]);
};

exports.getLimitPoolExceeded = async (conn) => {
  const collection = conn.connection.collection('assetsimcards');
  return collection.aggregate([
    {
      $match: {
        status: { $ne: 'inactive' },
      },
    },
    {
      $unwind: {
        path: '$subscriptions',
      },
    },
    {
      $match: {
        'subscriptions.bundles.0': { $exists: true },
      },
    },
    {
      $unwind: {
        path: '$subscriptions.bundles',
      },
    },
    {
      $group: {
        _id: '$subscriptions.bundles.localProductId',
        totalLimit: { $sum: '$limit' },
        totalDataUsed: { $sum: '$subscriptions.bundles.dataUsed' },
        totalSims: { $sum: 1 },
      },
    },
    {
      $match: {
        $expr: {
          $gt: ['$totalDataUsed', { $multiply: ['$totalLimit', 1.1] }],
        },
      },
    },
  ]);
};

exports.getProductType = async (data, conn) => {
  const arrValues = [];
  const collection = conn.connection.collection('productnews');
  for (const item of data) {
    const dataProducts = await collection.findOne({
      productType: {
        $in: ['Aggregate', 'SharedDataPool'],
      },
      _id: ObjectId(item._id),
    });

    if (dataProducts) {
      const finalObj = { ...item, ...dataProducts };
      finalObj.network = finalObj.networks[finalObj.networks.imsisType];
      const { productTypeScheme } = finalObj;
      if (productTypeScheme.Aggregate) {
        finalObj.poolAmount = finalObj.totalSims * finalObj.productTypeScheme[finalObj.productType].initialSize;
      } else {
        finalObj.poolAmount = finalObj.productTypeScheme[finalObj.productType].initialSize;
      }
      if (finalObj.poolAmount !== 0) {
        finalObj.poolUtilization = (finalObj.totalDataUsed / finalObj.poolAmount) * 100;
      } else {
        finalObj.poolUtilization = 0;
      }
      arrValues.push(finalObj);
    }
  }

  return arrValues;
};

exports.getValuesByAccount = async (conn, assets) => {
  const arrValues = [];
  const emails = new Map();
  let account;

  console.log('...Iterating through each SIM suspended, be patient');
  for (const item of assets) {
    const {
      suspensionDate, iccid, ownerAccountId, limit,
    } = item;
    const collectionAccounts = conn.connection.collection('accounts');
    const collectionCDRS = conn.connection.collection('cdrs');
    const email = emails.get(ownerAccountId);

    if (email === undefined) {
      account = await collectionAccounts.findOne({ id: ownerAccountId });
      emails.set(ownerAccountId, account.defaultEmail);
    }

    const cursorCDR = collectionCDRS.aggregate([
      {
        $match: {
          accountId: ownerAccountId,
          iccid,
          $expr: { $gt: ['$data.endTime', suspensionDate] },
        },
      },
      {
        $group: {
          _id: '$data.carrier',
          count: { $sum: 1 },
          bytes: { $sum: '$data.roundedBytes' },
        },
      }]);
    
    if (cursorCDR && await cursorCDR.hasNext()) {
      await cursorCDR.forEach((elem) => {
        arrValues.push({
          ICCID: iccid,
          accountId: ownerAccountId,
          defaultEmail: account.defaultEmail,
          network: elem._id,
          amountCDR: elem.count,
          bytes: elem.bytes/unitMB,
          accountName: account.name,
          limit,
        });
      });
    }
  }

  return arrValues;
};

exports.getValuesByProductType = async (conn, data) => {
  const arrValues = [];
  const accounts = new Map();
  let megaBytes = 0;
  for (const item of data) {
    const {
      accountTransferId, name, network, totalDataUsed,
      productType, poolAmount, poolUtilization,
    } = item;
    const collectionAccounts = conn.connection.collection('accounts');
    let account = accounts.get(accountTransferId);

    if (account === undefined) {
      account = await collectionAccounts.findOne({ id: accountTransferId }, { name: 1, defaultEmail: 1 });
      accounts.set(accountTransferId, account);
    }
    
    megaBytes = poolAmount / unitMB;

    arrValues.push({
      accountName: account.name,
      defaultEmail: account.defaultEmail,
      network,
      name,
      productType,
      totalDataUsed,
      poolAmount: megaBytes,
      poolUtilization,
    });
  }

  return arrValues;
};

exports.getChildrenAccounts = async (conn) => {
  console.log('...Getting all children accounts');
  const arrAccounts = [];
  const collectionAccounts = conn.connection.collection('accounts');
  const data = collectionAccounts.find({ resellerId: process.env.ACCOUNT_ID },
    {
      projection: {
        _id: 0, id: 1, name: 1, defaultEmail: 1,
      },
    });

  if (data && await data.hasNext()) {
    await data.forEach((elem) => {
      arrAccounts.push({ id: elem.id, accountName: elem.name, defaultEmail: elem.defaultEmail });
    });
  }

  return arrAccounts;
};

exports.getProductName = async (conn, results) => {
  const arrValues = [];
  const collection = conn.connection.collection('productnews');
  for (const item of results) {
    const data = await collection.findOne({ bundleId: item.bundleId },
      {
        projection: {
          name: 1,
          bundleId: 1,
          networks: 1,
          accountTransferId: 1,
        },
      });

    arrValues.push({
      name: data.name,
      bundleId: data.bundleId,
      network: data.networks[data.networks.imsisType],
      accountTransferId: data.accountTransferId,
      variation: item.variation,
    });
  }

  return arrValues;
};
