const chai = require('chai');
const sinon = require('sinon');

const { expect } = chai;
const db = require('.');
const query = require('./queries');
const stubSuspendedByUser = require('../mocks/suspendedByUser.json');

describe('get SIMS suspended', () => {
  let conn;
  let spyUser;
  beforeEach(async () => {
    if (!conn) {
      try {
        conn = await db.connect();
      } catch (e) {
        console.log(e, 'unable to simulate connection');
      }
    }
  });

  it('should get the SIMs already suspended by user with usage after', async () => {
    const stubValue = [stubSuspendedByUser];
    const collectionUser = conn.connection.collection('assetsimcards');
    spyUser = sinon.stub(collectionUser, 'find').returns(stubValue);
    const suspensionsByUser = await query.getSuspendedByUser(conn);
    expect(suspensionsByUser.smsLimit).to.equal(stubValue.smsLimit);
    expect(suspensionsByUser.status).to.equal(stubValue.status);
    expect(suspensionsByUser.limit).to.equal(stubValue.limit);
    expect(suspensionsByUser.iccid).to.equal(stubValue.iccid);
  });

  it('should get the SIMs already suspended by limits with usage after', async () => {
    // TODO: Change for the other mock (limit) when we have data
    const stubValue = [stubSuspendedByUser];
    const collectionLimit = conn.connection.collection('assetsimcards');
    spyUser.restore();
    sinon.stub(collectionLimit, 'find').returns(stubValue);
    const suspensionsByLimit = await query.getSuspendedByLimit(conn);
    expect(suspensionsByLimit.smsLimit).to.equal(stubValue.smsLimit);
    expect(suspensionsByLimit.status).to.equal(stubValue.status);
    expect(suspensionsByLimit.limit).to.equal(stubValue.limit);
    expect(suspensionsByLimit.iccid).to.equal(stubValue.iccid);
  });
});
